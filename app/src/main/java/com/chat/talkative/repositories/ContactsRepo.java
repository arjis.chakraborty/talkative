package com.chat.talkative.repositories;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.chat.talkative.activities.ChatActivity;
import com.chat.talkative.apiUtils.APIEndpoints;
import com.chat.talkative.apiUtils.RequestAPI;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.Talkative;
import com.chat.talkative.databases.daos.ContactsDAO;
import com.chat.talkative.databases.daos.RoomsDAO;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.interfaces.OnResponse;
import com.chat.talkative.services.SocketService;
import com.chat.talkative.viewModels.ContactsViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ContactsRepo {
    private ContactsDAO contactsDAO;
    private RoomsDAO roomsDAO;
    private LiveData<List<ContactsData>> contactsData;
    private Application application;
    String to_uid, room_id;
    private LiveData<RoomsData> room;
    private LiveData<ContactsData> contact;
    
    public ContactsRepo(Application application) {
        Talkative db = Talkative.getInstance(application);
        contactsDAO = db.contactsDAO();
        roomsDAO = db.roomsDAO();
        this.application = application;
        contactsData = contactsDAO.getAllContacts();
    }
    
    public LiveData<ContactsData> getOneUsingId(String id){
        return contactsDAO.getOneUsingId(id);
    }
    
    public void insertOne(ContactsData contactsData) {
        new InsertOne(contactsDAO).execute(contactsData);
    }
    
    public LiveData<ContactsData> getOne(String phNum) {
        return contactsDAO.getOne(phNum);
    }
    
    public void updateDisplayName(String phNum, String displayName) {
        new UpdateDisplayName(contactsDAO).execute(phNum, displayName);
    }
    
    public void updatePicURL(String phNum, String picURL) {
        new UpdatePicURL(contactsDAO).execute(phNum, picURL);
    }
    
    public void updateStatus(String id, String status){
        contactsDAO.updateStatus(id, status);
    }
    
    public void deleteAll() {
        new DeleteAll(contactsDAO).execute();
    }
    
    public LiveData<List<ContactsData>> getAllContacts(boolean forceRefresh) {
        if (!forceRefresh)
            contactsData.observeForever(contactsObserver);
        else
            setUserData();
        return contactsData;
    }
    
    public LiveData<RoomsData> getRoomDetails(String to_uid){
        return roomsDAO.getOneRoom(to_uid);
    }
    
    private Observer<List<ContactsData>> contactsObserver = new Observer<List<ContactsData>>() {
        @Override
        public void onChanged(List<ContactsData> contactsData) {
            if (contactsData.size() == 0) {
                setUserData();
            }
            ContactsRepo.this.contactsData.removeObserver(this);
        }
    };
    
    private Observer<RoomsData> roomsDataObserver = new Observer<RoomsData>() {
        @Override
        public void onChanged(RoomsData roomsData) {
            if (roomsData != null) {
                ContactsRepo.this.room_id = roomsData.getId();
                Intent intent = new Intent(application, SocketService.class);
                intent.setAction("JOIN");
                intent.putExtra("room_id", room_id);
                application.startService(intent);
            }
            ContactsRepo.this.room.removeObserver(this);
        }
    };
    
    private static class InsertOne extends AsyncTask<ContactsData, Void, Void> {
        private ContactsDAO contactsDAO;
        
        private InsertOne(ContactsDAO contactsDAO) {
            this.contactsDAO = contactsDAO;
        }
        
        @Override
        protected Void doInBackground(ContactsData... contactsData) {
            contactsDAO.insertOne(contactsData[0]);
            return null;
        }
    }
    
    private static class UpdateDisplayName extends AsyncTask<String, Void, Void> {
        private ContactsDAO contactsDAO;
        
        private UpdateDisplayName(ContactsDAO contactsDAO) {
            this.contactsDAO = contactsDAO;
        }
        
        @Override
        protected Void doInBackground(String... strings) {
            contactsDAO.updateDisplayName(strings[0], strings[1]);
            return null;
        }
    }
    
    private static class UpdatePicURL extends AsyncTask<String, Void, Void> {
        private ContactsDAO contactsDAO;
        
        private UpdatePicURL(ContactsDAO contactsDAO) {
            this.contactsDAO = contactsDAO;
        }
        
        @Override
        protected Void doInBackground(String... strings) {
            contactsDAO.updatePicUrl(strings[0], strings[1]);
            return null;
        }
    }
    
    private static class DeleteAll extends AsyncTask<Void, Void, Void> {
        private ContactsDAO contactsDAO;
        
        private DeleteAll(ContactsDAO contactsDAO) {
            this.contactsDAO = contactsDAO;
        }
        
        @Override
        protected Void doInBackground(Void... voids) {
            contactsDAO.deleteAll();
            return null;
        }
    }
    
    private String getContactName(final String phoneNumber, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        
        String contactName = "";
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
            }
            cursor.close();
        }
        
        return contactName;
    }
    
    private ArrayList<PhoneContactInfo> getAllPhoneContacts() {
        Log.e("START", "Getting all Contacts");
        ArrayList<PhoneContactInfo> arrContacts = new ArrayList<PhoneContactInfo>();
        PhoneContactInfo phoneContactInfo = null;
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        Cursor cursor = application.getContentResolver().query(uri, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone._ID}, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            String contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            int phoneContactID = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
            
            Log.e("PHNUM", contactName + ", " + contactNumber);
            phoneContactInfo = new PhoneContactInfo();
            phoneContactInfo.setPhoneContactId(phoneContactID + "");
            phoneContactInfo.setContactName(contactName);
            phoneContactInfo.setContactNumber(contactNumber.replaceAll(" ", ""));
            if (phoneContactInfo != null) {
                arrContacts.add(phoneContactInfo);
            }
            phoneContactInfo = null;
            cursor.moveToNext();
        }
        cursor.close();
        cursor = null;
        Log.e("END", "Got all Contacts");
        return arrContacts;
    }
    
    private static class PhoneContactInfo {
        String phoneContactId, contactName, contactNumber;
        
        public String getPhoneContactId() {
            return phoneContactId;
        }
        
        public void setPhoneContactId(String phoneContactId) {
            this.phoneContactId = phoneContactId;
        }
        
        public String getContactName() {
            return contactName;
        }
        
        public void setContactName(String contactName) {
            this.contactName = contactName;
        }
        
        public String getContactNumber() {
            return contactNumber;
        }
        
        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }
    }
    
    public void setUserData() {
        ArrayList<PhoneContactInfo> contactList = getAllPhoneContacts();
        deleteAll();
        JSONArray array = new JSONArray();
        for (int i = 0; i < contactList.size(); i++) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("phNum", contactList.get(i).getContactNumber());
                array.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        
        JSONObject body = new JSONObject();
        try {
            body.put("body", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        RequestAPI requestAPI = new RequestAPI(APIEndpoints.GET_ALL_CONTACTS, application);
        requestAPI.post(body, new OnResponse() {
            @Override
            public void onResponseReceived(String response) {
                try {
                    Log.e("RESP", response);
                    JSONObject body = new JSONObject(response);
                    String code = body.optString("code");
                    
                    if (code.equals("200")) {
                        JSONArray msg = body.optJSONArray("msg");
                        for (int j = 0; j < msg.length(); j++) {
                            JSONObject obj = msg.optJSONObject(j);
                            ContactsData contactsData = new ContactsData();
                            contactsData.setId(obj.optString("id"));
                            contactsData.setDisplayName(getContactName(obj.optString("phNum"), application));
                            contactsData.setName(obj.optString("name"));
                            contactsData.setPhNum(obj.optString("phNum"));
                            contactsData.setLogged_in(obj.optString("loggedIn"));
                            contactsData.setStatus(obj.optString("status"));
                            contactsData.setPicURL(obj.optString("picURL"));
                            contactsData.setCreated(obj.optString("created"));
                            contactsData.setPub_key(obj.optString("pub_key"));
                            insertOne(contactsData);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            
            @Override
            public void onErrorReceived(String error) {
            
            }
        });
    }
    
    public void removeObservers() {
        if (room != null)
            room.removeObserver(roomsDataObserver);
        if (contactsData != null)
            contactsData.removeObserver(contactsObserver);
    }
}
