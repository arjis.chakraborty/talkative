package com.chat.talkative.repositories;

import android.app.Application;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.chat.talkative.apiUtils.APIEndpoints;
import com.chat.talkative.apiUtils.RequestAPI;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.Talkative;
import com.chat.talkative.databases.daos.ChatsDAO;
import com.chat.talkative.databases.daos.ContactsDAO;
import com.chat.talkative.databases.daos.RoomsDAO;
import com.chat.talkative.databases.entities.ChatsData;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.interfaces.OnResponse;
import com.chat.talkative.services.SocketService;

import org.json.JSONException;
import org.json.JSONObject;

public class ChatsRepo {
    private ChatsDAO chatsDAO;
    private RoomsDAO roomsDAO;
    private ContactsDAO contactsDAO;
    private Application application;
    private Intent chatIntent;
    private LiveData<ContactsData> contactsData;
    private LiveData<RoomsData> room;
    
    public ChatsRepo(Application application) {
        Talkative db = Talkative.getInstance(application);
        this.application = application;
        chatsDAO = db.chatsDAO();
        roomsDAO = db.roomsDAO();
        contactsDAO = db.contactsDAO();
    }
    
    public long insertOne(ChatsData chatsData) {
        return chatsDAO.insertOne(chatsData);
    }
    
    public void updateStatus(String id, String status) {
        new UpdateStatus(chatsDAO).execute(id, status);
    }
    
    public LiveData<ChatsData> getOne(String id) {
        return chatsDAO.getOne(id);
    }
    
    public void deleteAll() {
        new DeleteAll(chatsDAO).execute();
    }
    
    public LiveData<PagedList<ChatsData>> getAllChats(String roomId) {
        return new LivePagedListBuilder<>(chatsDAO.getAllChats(roomId), 10).build();
    }
    
    public void createRoomAndSendMessage(String phNum, Intent chatIntent) {
        this.chatIntent = chatIntent;
        contactsData = contactsDAO.getOne(phNum);
        contactsData.observeForever(contactsDataObserver);
    }
    
    public void sendMessage(Intent intent) {
        application.startService(intent);
    }
    
    public void updateRoomId(String id, String roomId) {
        new UpdateRoomId(chatsDAO).execute(id, roomId);
    }
    
    public void joinRoom(String roomId) {
        Intent intent = new Intent(application, SocketService.class);
        intent.setAction("JOIN");
        intent.putExtra("room_id", roomId);
        application.startService(intent);
    }
    
    public void updateRoomStatus(String room_id, String room_status){
        roomsDAO.updateRoomStatus(room_id, room_status);
    }
    
    public void sendRoomStatus(String room_id, String room_status){
        Intent intent = new Intent(application, SocketService.class);
        intent.setAction("UPDATE_ROOM_STATUS");
        intent.putExtra("room_id", room_id);
        intent.putExtra("room_status", room_status);
        application.startService(intent);
    }
    
    public LiveData<ContactsData> getContactDetails(String id) {
        return contactsDAO.getOneUsingId(id);
    }
    
    private String roomId, to_uid, from_uid, created;
    private Observer<ContactsData> contactsDataObserver = new Observer<ContactsData>() {
        @Override
        public void onChanged(ContactsData contactsData) {
            String id = contactsData.getId();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("to_uid", id);
                jsonObject.put("from_uid", Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, ""));
                
                RequestAPI requestAPI = new RequestAPI(APIEndpoints.CREATE_ROOM, application);
                requestAPI.post(jsonObject, new OnResponse() {
                    @Override
                    public void onResponseReceived(String response) {
                        try {
                            JSONObject body = new JSONObject(response);
                            String code = body.optString("code");
                            if (code.equals("200")) {
                                JSONObject msg = body.optJSONObject("msg");
                                roomId = msg.optString("id");
                                to_uid = msg.optString("to_uid");
                                from_uid = msg.optString("from_uid");
                                created = msg.optString("created");
                                
                                ChatsRepo.this.contactsData = contactsDAO.getOneUsingId(to_uid);
                                ChatsRepo.this.contactsData.observeForever(contactsDataObserver2);
                            }
                            
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    
                    @Override
                    public void onErrorReceived(String error) {
                        Log.e("ERROR", error);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ChatsRepo.this.contactsData.removeObserver(this);
        }
    };
    
    private Observer<ContactsData> contactsDataObserver2 = new Observer<ContactsData>() {
        @Override
        public void onChanged(ContactsData contactsData) {
            RoomsData data = new RoomsData();
            data.setId(roomId);
            data.setTo_uid(to_uid);
            data.setFrom_uid(from_uid);
            data.setLast_message("");
            data.setTo_name(contactsData.getDisplayName());
            data.setCreated(created);
            
            roomsDAO.insertOne(data);
            
            Intent intent = new Intent(application, SocketService.class);
            intent.setAction("JOIN");
            intent.putExtra("room_id", data.getId());
            application.startService(intent);
            
            chatIntent.putExtra("room_id", data.getId());
            
            sendMessage(chatIntent);
            ChatsRepo.this.contactsData.removeObserver(this);
            //roomCreated(roomCreatedIntent);
        }
    };
    
    private static class InsertOne extends AsyncTask<ChatsData, Void, Void> {
        private ChatsDAO chatsDAO;
        
        private InsertOne(ChatsDAO chatsDAO) {
            this.chatsDAO = chatsDAO;
        }
        
        @Override
        protected Void doInBackground(ChatsData... chatsData) {
            chatsDAO.insertOne(chatsData[0]);
            return null;
        }
    }
    
    private static class UpdateStatus extends AsyncTask<String, Void, Void> {
        private ChatsDAO chatsDAO;
        
        private UpdateStatus(ChatsDAO chatsDAO) {
            this.chatsDAO = chatsDAO;
        }
        
        @Override
        protected Void doInBackground(String... strings) {
            chatsDAO.updateStatus(strings[0], strings[1]);
            return null;
        }
    }
    
    private static class DeleteAll extends AsyncTask<Void, Void, Void> {
        private ChatsDAO chatsDAO;
        
        private DeleteAll(ChatsDAO chatsDAO) {
            this.chatsDAO = chatsDAO;
        }
        
        @Override
        protected Void doInBackground(Void... voids) {
            chatsDAO.deleteAll();
            return null;
        }
    }
    
    private static class UpdateRoomId extends AsyncTask<String, Void, Void> {
        private ChatsDAO chatsDAO;
        
        private UpdateRoomId(ChatsDAO chatsDAO) {
            this.chatsDAO = chatsDAO;
        }
        
        @Override
        protected Void doInBackground(String... strings) {
            chatsDAO.updateRoomId(strings[0], strings[1]);
            return null;
        }
    }
    
    public void removeObservers() {
        if (contactsData != null)
            contactsData.removeObserver(contactsDataObserver);
    }
}
