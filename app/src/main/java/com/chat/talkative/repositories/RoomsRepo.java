package com.chat.talkative.repositories;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.chat.talkative.activities.ChatActivity;
import com.chat.talkative.apiUtils.APIEndpoints;
import com.chat.talkative.apiUtils.RequestAPI;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.Talkative;
import com.chat.talkative.databases.daos.ChatsDAO;
import com.chat.talkative.databases.daos.ContactsDAO;
import com.chat.talkative.databases.daos.RoomsDAO;
import com.chat.talkative.databases.entities.ChatsData;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.encryption.AES;
import com.chat.talkative.encryption.RSA;
import com.chat.talkative.interfaces.OnResponse;
import com.chat.talkative.services.SocketService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static android.content.Context.MODE_PRIVATE;

public class RoomsRepo {
    private RoomsDAO roomsDAO;
    private ChatsDAO chatsDAO;
    private LiveData<List<RoomsData>> roomsData;
    private Application application;
    private ContactsDAO contactsDAO;
    private RoomsData roomData;
    private LiveData<ContactsData> contactsData;
    private LiveData<RoomsData> roomsDataLiveData;
    private LiveData<ChatsData> chatData;
    
    public RoomsRepo(Application application) {
        Talkative db = Talkative.getInstance(application);
        this.application = application;
        roomsDAO = db.roomsDAO();
        chatsDAO = db.chatsDAO();
        contactsDAO = db.contactsDAO();
        roomsData = roomsDAO.getAllRooms();
        Intent intent = new Intent(application, SocketService.class);
        intent.setAction("CONNECT");
        application.startService(intent);
    }
    
    public void onDestroy(Intent intent){
        application.startService(intent);
    }
    
    public LiveData<RoomsData> getOneUsingId(String roomId){
        return roomsDAO.getOneRoomUsingId(roomId);
    }
    
    public void insertOne(RoomsData roomsData) {
        new InsertOne(roomsDAO).execute(roomsData);
    }
    
    public void updateLastMessage(String roomId, String lastMessage) {
        new UpdateLastMessage(roomsDAO).execute(roomId, lastMessage);
    }
    
    public void deleteAll() {
        new DeleteAll(roomsDAO).execute();
    }
    
    public LiveData<List<RoomsData>> getAllRooms() {
        setUserData();
        return roomsData;
    }
    
    private String id, from_uid, to_uid, last_message, created, last_message_id;
    void getAllMessages() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", application.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.LAST_MESSAGE_RECEIVED_ID, "1"));
            jsonObject.put("uid", application.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""));
            
            RequestAPI requestAPI = new RequestAPI(APIEndpoints.GET_ALL_NEW_MESSAGES, application);
            requestAPI.post(jsonObject, new OnResponse() {
                @Override
                public void onResponseReceived(String response) {
                    try {
                        JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        if (code.equals("200")) {
                            JSONArray msg = body.optJSONArray("msg");
                            for (int i = 0; i < msg.length(); i++) {
                                JSONObject obj = msg.optJSONObject(i);
                                ChatsData data = new ChatsData();
                                data.setId(obj.optString("id"));
                                data.setStatus(obj.optString("status"));
                                data.setIsDeleted(obj.optString("isDeleted"));
                                data.setRoom_id(obj.optString("roomId"));
                                data.setTo_uid(obj.optString("to_uid"));
                                data.setFrom_uid(obj.optString("from_uid"));
                                byte[] IV = Base64.decode(obj.optString("iv"), Base64.DEFAULT);
                                String aes_key = obj.optString("aes_key");
                                data.setMessage(decrypt(aes_key, obj.optString("message"), IV));
                                
                                data.setCreated(obj.optString("created"));
                                
                                chatsDAO.insertOne(data);
                                
                                JSONObject roomData = obj.optJSONObject("roomData");
                                id = roomData.optString("id");
                                from_uid = roomData.optString("from_uid");
                                to_uid = roomData.optString("to_uid");
                                last_message = data.getMessage();
                                last_message_id = data.getId();
                                created = roomData.optString("created");
                                roomsDataLiveData =  roomsDAO.getOneRoomUsingId((roomData.optString("id")));
                                roomsDataLiveData.observeForever(roomsDataObserver);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                
                @Override
                public void onErrorReceived(String error) {
                    Log.e("VOLLEY", error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    String decrypt(String aesKey, String msg, byte[] IV) {
        try {
            byte[] decodeData = RSA.decryptByPrivateKey(Base64.decode(aesKey, Base64.DEFAULT), application.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_PRV_KEY, ""));
            decodeData = new String(decodeData).replaceAll("\n", "").replaceAll(" ", "").getBytes();
            byte[] decodedKey = Base64.decode(decodeData, Base64.DEFAULT);
            SecretKey aes = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
            return AES.decrypt(Base64.decode(msg, Base64.DEFAULT), aes, IV);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    String encryptKey(String aesKey, String pub_key) {
        byte[] aes_key = aesKey.getBytes();
        byte[] encodeData = null;
        try {
            encodeData = RSA.encryptByPublicKey(aes_key, pub_key);
            return Base64.encodeToString(encodeData, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    String encryptMessage(String message, String aesKey, byte[] IV) {
        byte[] encodedKey = android.util.Base64.decode(aesKey, android.util.Base64.DEFAULT);
        SecretKey aes = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        try {
            byte[] encrypt = AES.encrypt(message.getBytes(), aes, IV);
            return Base64.encodeToString(encrypt, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    void getRoomsFromRemoteDB() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("from_uid", application.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""));
            
            RequestAPI requestAPI = new RequestAPI(APIEndpoints.GET_ALL_ROOMS, application);
            requestAPI.post(jsonObject, new OnResponse() {
                @Override
                public void onResponseReceived(String response) {
                    try {
                        JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        if (code.equals("200")) {
                            JSONArray msg = body.optJSONArray("msg");
                            for (int i = 0; i < msg.length(); i++) {
                                JSONObject obj = msg.optJSONObject(i);
                                
                                id = obj.optString("id");
                                from_uid = obj.optString("from_uid");
                                to_uid = obj.optString("to_uid");
                                created = obj.optString("created");
                                last_message_id = obj.optString("last_message_id");
                                
                                contactsData = contactsDAO.getOneUsingId(from_uid.equals(Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, "")) ? to_uid : from_uid);
                                Log.e("ID", from_uid.equals(Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, "")) ? "TO" + to_uid : "TO" + from_uid);
                                contactsData.observeForever(contactsDataObserver3);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                
                @Override
                public void onErrorReceived(String error) {
                    Log.e("ERROR", error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    public ContactsData getOne(String to_uid) {
        return roomsDAO.getOneContact(to_uid);
    }
    
    public void joinRoom(RoomsData roomsData) {
        if (roomsData != null) {
            //this.roomData = roomsData;
            Intent intent = new Intent(application, SocketService.class);
            intent.setAction("JOIN");
            intent.putExtra("room_id", roomsData.getId());
            application.startService(intent);
            
            /*contactsData = contactsDAO.getOneUsingId(roomsData.getTo_uid());
            contactsData.observeForever(contactsDataObserver);*/
        }
    }
    
    RoomsData data;
    
    private Observer<ContactsData> contactsDataObserver3 = new Observer<ContactsData>() {
        @Override
        public void onChanged(ContactsData contactsData) {
            data = new RoomsData();
            data.setId(id);
            data.setFrom_uid(from_uid);
            data.setTo_uid(to_uid);
            data.setCreated(created);
            data.setTo_name(contactsData.getDisplayName());
            
            chatData = chatsDAO.getOne(last_message_id);
            chatData.observeForever(chatsDataObserver);
        }
    };
    
    private Observer<ChatsData> chatsDataObserver = new Observer<ChatsData>() {
        @Override
        public void onChanged(ChatsData chatsData) {
            data.setLast_message(chatsData.getMessage());
            insertOne(data);
            chatData.removeObserver(this);
        }
    };
    
    private Observer<RoomsData> roomsDataObserver = new Observer<RoomsData>() {
        @Override
        public void onChanged(RoomsData roomsData) {
            if (roomsData != null) {
                joinRoom(roomsData);
                updateLastMessage(id, last_message);
            } else {
                contactsData = contactsDAO.getOneUsingId(to_uid.equals(Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, "")) ? from_uid : to_uid);
                contactsData.observeForever(contactsDataObserver2);
            }
            application.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).edit().putString(Constants.LAST_MESSAGE_RECEIVED_ID, last_message_id + "").apply();
            roomsDataLiveData.removeObserver(this);
        }
    };
    
    private Observer<ContactsData> contactsDataObserver2 = new Observer<ContactsData>() {
        @Override
        public void onChanged(ContactsData contactsData) {
            RoomsData newRoom = new RoomsData();
            newRoom.setId(id);
            newRoom.setFrom_uid(from_uid);
            newRoom.setTo_uid(to_uid);
            newRoom.setTo_name(contactsData.getDisplayName());
            newRoom.setLast_message(last_message);
            newRoom.setCreated(created);
            roomsDAO.insertOne(newRoom);
            
            joinRoom(newRoom);
            RoomsRepo.this.contactsData.removeObserver(this);
        }
    };
    
    private static class InsertOne extends AsyncTask<RoomsData, Void, Void> {
        private RoomsDAO roomsDAO;
        
        private InsertOne(RoomsDAO roomsDAO) {
            this.roomsDAO = roomsDAO;
        }
        
        @Override
        protected Void doInBackground(RoomsData... roomsData) {
            roomsDAO.insertOne(roomsData[0]);
            return null;
        }
    }
    
    private static class UpdateLastMessage extends AsyncTask<String, Void, Void> {
        private RoomsDAO roomsDAO;
        
        private UpdateLastMessage(RoomsDAO roomsDAO) {
            this.roomsDAO = roomsDAO;
        }
        
        @Override
        protected Void doInBackground(String... strings) {
            roomsDAO.updateLastMessage(strings[0], strings[1]);
            return null;
        }
    }
    
    private static class DeleteAll extends AsyncTask<Void, Void, Void> {
        private RoomsDAO roomsDAO;
        
        private DeleteAll(RoomsDAO roomsDAO) {
            this.roomsDAO = roomsDAO;
        }
        
        @Override
        protected Void doInBackground(Void... voids) {
            roomsDAO.deleteAll();
            return null;
        }
    }
    
    private String getContactName(final String phoneNumber, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        
        String contactName = "";
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
            }
            cursor.close();
        }
        
        return contactName;
    }
    
    private ArrayList<PhoneContactInfo> getAllPhoneContacts() {
        Log.e("START", "Getting all Contacts");
        ArrayList<PhoneContactInfo> arrContacts = new ArrayList<PhoneContactInfo>();
        PhoneContactInfo phoneContactInfo = null;
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        Cursor cursor = application.getContentResolver().query(uri, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone._ID}, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            String contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            int phoneContactID = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
            
            Log.e("PHNUM", contactName + ", " + contactNumber);
            phoneContactInfo = new PhoneContactInfo();
            phoneContactInfo.setPhoneContactId(phoneContactID + "");
            phoneContactInfo.setContactName(contactName);
            phoneContactInfo.setContactNumber(contactNumber.replaceAll(" ", ""));
            if (phoneContactInfo != null) {
                arrContacts.add(phoneContactInfo);
            }
            phoneContactInfo = null;
            cursor.moveToNext();
        }
        cursor.close();
        cursor = null;
        Log.e("END", "Got all Contacts");
        return arrContacts;
    }
    
    private static class PhoneContactInfo {
        String phoneContactId, contactName, contactNumber;
        
        public String getPhoneContactId() {
            return phoneContactId;
        }
        
        public void setPhoneContactId(String phoneContactId) {
            this.phoneContactId = phoneContactId;
        }
        
        public String getContactName() {
            return contactName;
        }
        
        public void setContactName(String contactName) {
            this.contactName = contactName;
        }
        
        public String getContactNumber() {
            return contactNumber;
        }
        
        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }
    }
    
    public void setUserData() {
        ArrayList<PhoneContactInfo> contactList = getAllPhoneContacts();
        //deleteAll();
        JSONArray array = new JSONArray();
        for (int i = 0; i < contactList.size(); i++) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("phNum", contactList.get(i).getContactNumber());
                array.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        
        JSONObject body = new JSONObject();
        try {
            body.put("body", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        RequestAPI requestAPI = new RequestAPI(APIEndpoints.GET_ALL_CONTACTS, application);
        requestAPI.post(body, new OnResponse() {
            @Override
            public void onResponseReceived(String response) {
                try {
                    Log.e("RESP", response);
                    JSONObject body = new JSONObject(response);
                    String code = body.optString("code");
                    
                    if (code.equals("200")) {
                        JSONArray msg = body.optJSONArray("msg");
                        for (int j = 0; j < msg.length(); j++) {
                            JSONObject obj = msg.optJSONObject(j);
                            ContactsData contactsData2 = new ContactsData();
                            contactsData2.setId(obj.optString("id"));
                            contactsData2.setDisplayName(getContactName(obj.optString("phNum"), application));
                            contactsData2.setName(obj.optString("name"));
                            contactsData2.setPhNum(obj.optString("phNum"));
                            contactsData2.setLogged_in(obj.optString("loggedIn"));
                            String status = obj.optString("status");
                            if(Character.isDigit(status.charAt(0))){
                                try {
                                    status = "last seen at " + formatTime(status);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            contactsData2.setStatus(status);
                            contactsData2.setPicURL(obj.optString("picURL"));
                            contactsData2.setCreated(obj.optString("created"));
                            contactsData2.setPub_key(obj.optString("pub_key"));
                            contactsDAO.insertOne(contactsData2);
                        }
    
                        getAllMessages();
                        
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            
            @Override
            public void onErrorReceived(String error) {
            
            }
        });
    }
    
    String formatTime(String time) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = df.parse(time);
        df.setTimeZone(TimeZone.getDefault());
        return df.format(date);
    }
    
    public void removeObservers() {
        if (contactsData != null) {
            //contactsData.removeObserver(contactsDataObserver);
            contactsData.removeObserver(contactsDataObserver2);
            contactsData.removeObserver(contactsDataObserver3);
        }
        if(roomsDataLiveData != null)
            roomsDataLiveData.removeObserver(roomsDataObserver);
        if(chatData != null)
            chatData.removeObserver(chatsDataObserver);
    }
}
