package com.chat.talkative.constants;

import android.content.SharedPreferences;

public class Constants {
    
    public static String DB_NAME = "talkative";
    
    public static SharedPreferences SHARED_PREFERENCES;
    public static String SHARED_PREFERENCE_NAME = "talkative";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_PH = "user_ph";
    public static final String USER_PIC = "user_pic";
    public static final String USER_LOGGED = "user_logged";
    public static final String USER_STATUS = "user_status";
    public static final String USER_CREATED = "user_created";
    public static final String USER_PUB_KEY = "user_public_key";
    public static final String USER_PRV_KEY = "user_private_key";
    public static final String LAST_MESSAGE_RECEIVED_ID = "last_message_id";
    
    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 10001;
    
    public static boolean REFRESH_ROOMS = false;
    
    public static boolean IS_SOCKET_ACTIVE = false;
}
