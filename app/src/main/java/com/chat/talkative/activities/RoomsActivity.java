package com.chat.talkative.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.chat.talkative.R;
import com.chat.talkative.adapters.RoomsRecyclerAdapter;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.interfaces.OnRoomClickListener;
import com.chat.talkative.services.SocketService;
import com.chat.talkative.viewModels.RoomsViewModel;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.chat.talkative.constants.Constants.PERMISSIONS_REQUEST_READ_CONTACTS;

public class RoomsActivity extends AppCompatActivity {
    
    private RoomsViewModel roomsViewModel;
    private RecyclerView roomsRecycler;
    private ExtendedFloatingActionButton fab;
    private RelativeLayout noDataLayout;
    private LiveData<List<RoomsData>> roomsListData;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);
        
        roomsRecycler = findViewById(R.id.roomsRecycler);
        roomsRecycler.setLayoutManager(new LinearLayoutManager(this));
        fab = findViewById(R.id.fab);
        noDataLayout = findViewById(R.id.noDataLayout);
        
        roomsViewModel = ViewModelProviders.of(this).get(RoomsViewModel.class);
        
        RoomsRecyclerAdapter adapter = new RoomsRecyclerAdapter(this, new OnRoomClickListener() {
            @Override
            public void onClick(View view, RoomsData roomsData) {
                if (roomsData != null) {
                    //Open ChatActivity and send necessary data
                    Intent intent = new Intent(RoomsActivity.this, ChatActivity.class);
                    intent.putExtra("room_id", roomsData.getId());
                    intent.putExtra("DISPLAY_NAME", roomsData.getTo_name());
                    if (roomsData.getTo_uid().equals(Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, ""))) {
                        intent.putExtra("to_uid", roomsData.getFrom_uid());
                    } else {
                        intent.putExtra("to_uid", roomsData.getTo_uid());
                    }
                    startActivity(intent);
                    
                    //Call RoomsViewModel to invoke socket to join the room
                    roomsViewModel.joinRoom(roomsData);
                }
            }
        });
        
        roomsRecycler.setAdapter(adapter);
        
        roomsListData = roomsViewModel.getAllRooms();
        roomsListData.observe(this, new Observer<List<RoomsData>>() {
            @Override
            public void onChanged(List<RoomsData> roomsDataList) {
                adapter.setRooms(roomsDataList);
                if (roomsDataList.size() == 0) {
                    noDataLayout.setVisibility(View.VISIBLE);
                } else {
                    noDataLayout.setVisibility(View.GONE);
                }
            }
        });
        
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestContactPermission();
            }
        });
    }
    
    public void requestContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Read contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("Please enable access to contacts.");
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {android.Manifest.permission.READ_CONTACTS}
                                    , PERMISSIONS_REQUEST_READ_CONTACTS);
                        }
                    });
                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_CONTACTS},
                            PERMISSIONS_REQUEST_READ_CONTACTS);
                }
            } else {
                Intent intent = new Intent(RoomsActivity.this, ContactsActivity.class);
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(RoomsActivity.this, ContactsActivity.class);
            startActivity(intent);
        }
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(RoomsActivity.this, ContactsActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "You denied the contacts permission", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(this, SocketService.class);
        intent.putExtra("id", Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, ""));
        intent.setAction("DESTROY");
    
        roomsViewModel.onDestroy(intent);
    
        Constants.IS_SOCKET_ACTIVE = false;
    }
}