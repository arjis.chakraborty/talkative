package com.chat.talkative.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.chat.talkative.R;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.services.SocketService;

public class SplashActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        
        Constants.SHARED_PREFERENCES = getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        
        new Handler()
                .postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, "").length() == 0) {
                            startLoginActivity();
                        } else {
                            Log.e("PUB_KEY", Constants.SHARED_PREFERENCES.getString(Constants.USER_PUB_KEY, ""));
                            startRoomsActivity();
                        }
                    }
                }, 1500);
    }
    
    void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    
    void startRoomsActivity() {
        Intent intent = new Intent(this, RoomsActivity.class);
        startActivity(intent);
        finish();
    }
}
