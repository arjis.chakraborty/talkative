package com.chat.talkative.activities;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.chat.talkative.R;
import com.chat.talkative.apiUtils.APIEndpoints;
import com.chat.talkative.apiUtils.RequestAPI;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.encryption.RSA;
import com.chat.talkative.interfaces.OnResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.chat.talkative.constants.Constants.PERMISSIONS_REQUEST_READ_CONTACTS;

public class LoginActivity extends AppCompatActivity {
    
    EditText phNum, name;
    Button lets_chat_btn;
    
    Map<String, Object> keyMap;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        phNum = findViewById(R.id.phNum_edit);
        name = findViewById(R.id.name_edit);
        lets_chat_btn = findViewById(R.id.submit_btn);
        
        lets_chat_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    saveData();
                } catch (Exception e) {
                    Log.e("ENCRYPTION", e.toString());
                }
            }
        });
        
    }
    
    public void requestContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Read contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("Please enable access to contacts.");
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {android.Manifest.permission.READ_CONTACTS}
                                    , PERMISSIONS_REQUEST_READ_CONTACTS);
                        }
                    });
                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_CONTACTS},
                            PERMISSIONS_REQUEST_READ_CONTACTS);
                }
            } else {
                Intent intent = new Intent(this, RoomsActivity.class);
                startActivity(intent);
                finish();
            }
        } else {
            Intent intent = new Intent(this, RoomsActivity.class);
            startActivity(intent);
            finish();
        }
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, RoomsActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, "You denied the contacts permission", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }
    
    void saveData() throws Exception {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", name.getText().toString());
            jsonObject.put("phNum", "+91" + phNum.getText().toString());
            jsonObject.put("picURL", "");
            
            keyMap = RSA.initKey();
            String publicKey = RSA.getPublicKey(keyMap);
            String privateKey = RSA.getPrivateKey(keyMap);
            
            Log.e("ENCRYPTION", publicKey + "\n" + privateKey);
            
            jsonObject.put("pub_key", publicKey);
            
            RequestAPI requestAPI = new RequestAPI(APIEndpoints.ADD_USER, this);
            requestAPI.post(jsonObject, new OnResponse() {
                @Override
                public void onResponseReceived(String response) {
                    try {
                        JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        
                        if (code.equals("200")) {
                            JSONObject msg = body.optJSONObject("msg");
                            assert msg != null;
                            storeData(msg, privateKey);
                            requestContactPermission();
                            
                        } else {
                            Toast.makeText(LoginActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        Log.e("VOLLEY", e.toString());
                    }
                }
                
                @Override
                public void onErrorReceived(String error) {
                    Log.e("ERROR", error);
                }
            });
        } catch (JSONException e) {
            Log.e("VOLLEY", e.toString());
        }
        
    }
    
    void storeData(JSONObject msg, String privateKey) {
        Constants.SHARED_PREFERENCES = getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_ID, msg.optString("id")).apply();
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_NAME, msg.optString("name")).apply();
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_PH, msg.optString("phNum")).apply();
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_PIC, msg.optString("picURL")).apply();
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_LOGGED, msg.optString("loggedIn")).apply();
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_STATUS, msg.optString("status")).apply();
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_CREATED, msg.optString("created")).apply();
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_PUB_KEY, msg.optString("pub_key")).apply();
        Constants.SHARED_PREFERENCES.edit().putString(Constants.USER_PRV_KEY, privateKey).apply();
    }
}
