package com.chat.talkative.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chat.talkative.R;
import com.chat.talkative.adapters.ChatsRecyclerAdapter;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.entities.ChatsData;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.encryption.AES;
import com.chat.talkative.encryption.RSA;
import com.chat.talkative.interfaces.OnRoomCreatedListener;
import com.chat.talkative.repositories.ChatsRepo;
import com.chat.talkative.services.SocketService;
import com.chat.talkative.viewModels.ChatsViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class ChatActivity extends AppCompatActivity {
    
    private ChatsViewModel chatsViewModel;
    private String room_id, displayName, status, to_uid, picURL, pub_key, phNum;
    private boolean didRoomExist = true;
    ChatsRecyclerAdapter adapter;
    RecyclerView chatsRecycler;
    EditText messageEdit;
    ImageView sendBtn;
    List<ChatsData> chatsData;
    Toolbar toolbar;
    TextView name, statusView;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);
        
        chatsRecycler = findViewById(R.id.chatsRecycler);
        messageEdit = findViewById(R.id.messageEdit);
        sendBtn = findViewById(R.id.sendBtn);
        toolbar = findViewById(R.id.toolbar);
        name = toolbar.findViewById(R.id.displayName);
        statusView = toolbar.findViewById(R.id.status);
        
        disableBtn();
        
        Intent intent = getIntent();
        room_id = intent.getStringExtra("room_id");
        displayName = intent.getStringExtra("DISPLAY_NAME");
        name.setText(displayName);
        
        to_uid = intent.getStringExtra("to_uid");
        didRoomExist = room_id != null && room_id.length() > 0;
        
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("RoomCreated"));
        LocalBroadcastManager.getInstance(this).registerReceiver(roomStatusReceiver, new IntentFilter("RoomStatusUpdated"));
        
        chatsViewModel = ViewModelProviders.of(this).get(ChatsViewModel.class);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        layoutManager.setReverseLayout(true);
        chatsRecycler.setLayoutManager(layoutManager);
        adapter = new ChatsRecyclerAdapter(this);
        chatsRecycler.setAdapter(adapter);
        
        chatsViewModel.getContactsDetails(to_uid).observe(this, new Observer<ContactsData>() {
            @Override
            public void onChanged(ContactsData contactsData) {
                status = contactsData.getStatus();
                statusView.setText(status);
                phNum = contactsData.getPhNum();
                picURL = contactsData.getPicURL();
                pub_key = contactsData.getPub_key();
            }
        });
        
        if (room_id != null) {
            chatsViewModel.getAllChats(room_id).observe(this, new Observer<PagedList<ChatsData>>() {
                @Override
                public void onChanged(PagedList<ChatsData> chatsData) {
                    Log.e("SIZE", chatsData.size() + "");
                    ChatActivity.this.chatsData = chatsData;
                    adapter.submitList(chatsData);
                    chatsRecycler.scrollToPosition(0);
                }
            });
            chatsViewModel.joinRoom(room_id);
        }
        
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
        
        messageEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() > 0) {
                    if (room_id != null) {
                        chatsViewModel.sendRoomStatus(room_id, "typing a message...");
                    }
                    enableBtn();
                } else {
                    if (room_id != null)
                        chatsViewModel.sendRoomStatus(room_id, "");
                }
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
            
            }
        });
        
        messageEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    new Handler()
                            .postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    chatsRecycler.scrollToPosition(0);
                                }
                            }, 150);
            }
        });
    }
    
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String room_id = intent.getStringExtra("roomId");
            if (!room_id.equals(ChatActivity.this.room_id)) {
                ChatActivity.this.room_id = room_id;
                if (chatsViewModel == null)
                    chatsViewModel = ViewModelProviders.of(ChatActivity.this).get(ChatsViewModel.class);
                chatsViewModel.getAllChats(room_id).observe(ChatActivity.this, new Observer<PagedList<ChatsData>>() {
                    @Override
                    public void onChanged(PagedList<ChatsData> chatsData) {
                        ChatActivity.this.chatsData = chatsData;
                        //adapter.setChats(chatsData);
                        adapter.submitList(chatsData);
                        chatsRecycler.smoothScrollToPosition(chatsData != null && chatsData.size() > 0 ? chatsData.size() - 1 : 0);
                    }
                });
                chatsViewModel.joinRoom(room_id);
            }
        }
    };
    
    private final BroadcastReceiver roomStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String room_id = intent.getStringExtra("room_id");
            String room_status = intent.getStringExtra("room_status");
            
            if (room_id.equals(ChatActivity.this.room_id)) {
                if (!room_status.equals(""))
                    statusView.setText(room_status);
                else
                    statusView.setText(status);
            }
        }
    };
    
    void disableBtn() {
        sendBtn.setImageResource(R.drawable.chat_send_disabled);
        sendBtn.setEnabled(false);
        sendBtn.setClickable(false);
    }
    
    void enableBtn() {
        sendBtn.setImageResource(R.drawable.chat_send);
        sendBtn.setEnabled(true);
        sendBtn.setClickable(true);
    }
    
    void sendMessage() {
        String message = messageEdit.getText().toString();
        
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String currentTime = sdf.format(new Date());
        
        String id = System.currentTimeMillis() + "";
        
        Intent intent = new Intent(this, SocketService.class);
        intent.setAction("SEND");
        intent.putExtra("to_uid", getIntent().getStringExtra("to_uid"));
        intent.putExtra("from_uid", getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""));
        
        //need to encrypt message first
        AES.init();
        AES.initPadding();
        SecretKey aesKey = AES.secretKey;
        byte[] IV = AES.IV;
        intent.putExtra("message", encryptMessage(message, AES.getStringKey(aesKey), IV));
        
        intent.putExtra("key", encryptKey(AES.getStringKey(aesKey), pub_key));
        intent.putExtra("IV", IV);
        intent.putExtra("created", currentTime);
        intent.putExtra("id", id);
        
        ChatsData chatsData = new ChatsData();
        chatsData.setId(id);
        chatsData.setRoom_id(room_id);
        try {
            chatsData.setCreated(formatTime(currentTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        chatsData.setFrom_uid(getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""));
        chatsData.setTo_uid(getIntent().getStringExtra("to_uid"));
        chatsData.setIsDeleted("0");
        chatsData.setMessage(message);
        chatsData.setStatus("pending");
        
        long msgID = chatsViewModel.insertOne(chatsData);
        
        if (msgID > 0)
            if (!didRoomExist) {
                intent.putExtra("isFirstMessage", true);
                chatsViewModel.createRoomAndSendMessage(phNum != null ? phNum : "", intent);
            } else {
                intent.putExtra("room_id", room_id);
                chatsViewModel.sendMessage(intent);
            }
        messageEdit.setText("");
        disableBtn();
        
    }
    
    String formatTime(String time) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = df.parse(time);
        df.setTimeZone(TimeZone.getDefault());
        return df.format(date);
    }
    
    String encryptKey(String aesKey, String pub_key) {
        byte[] aes_key = aesKey.getBytes();
        byte[] encodeData = null;
        try {
            encodeData = RSA.encryptByPublicKey(aes_key, pub_key);
            return Base64.encodeToString(encodeData, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    String encryptMessage(String message, String aesKey, byte[] IV) {
        byte[] encodedKey = android.util.Base64.decode(aesKey, android.util.Base64.DEFAULT);
        SecretKey aes = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        try {
            byte[] encrypt = AES.encrypt(message.getBytes(), aes, IV);
            return Base64.encodeToString(encrypt, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
