package com.chat.talkative.activities;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chat.talkative.R;
import com.chat.talkative.adapters.ContactsRecyclerAdapter;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.interfaces.OnContactClickListener;
import com.chat.talkative.viewModels.ContactsViewModel;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.util.List;

import static com.chat.talkative.constants.Constants.PERMISSIONS_REQUEST_READ_CONTACTS;

public class ContactsActivity extends AppCompatActivity {
    
    private ContactsViewModel contactsViewModel;
    private RecyclerView contactsRecycler;
    private ContactsRecyclerAdapter adapter;
    private ExtendedFloatingActionButton refreshBtn;
    private int counter = 0;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        
        contactsViewModel = ViewModelProviders.of(this).get(ContactsViewModel.class);
        
        requestContactPermission(false);
        
        contactsRecycler = findViewById(R.id.contactsRecycler);
        refreshBtn = findViewById(R.id.refreshBtn);
        contactsRecycler.setLayoutManager(new LinearLayoutManager(this));
        
        adapter = new ContactsRecyclerAdapter(this, new OnContactClickListener() {
            @Override
            public void onClick(ContactsData contactsData) {
                contactsViewModel.getRoom(contactsData.getId()).observe(ContactsActivity.this, new Observer<RoomsData>() {
                    @Override
                    public void onChanged(RoomsData roomsData) {
                        if (counter == 0) {
                            Intent intent = new Intent(ContactsActivity.this, ChatActivity.class);
                            if (roomsData != null) {
                                intent.putExtra("room_id", roomsData.getId());
                                intent.putExtra("DISPLAY_NAME", roomsData.getTo_name());
                                if (roomsData.getTo_uid().equals(Constants.SHARED_PREFERENCES.getString(Constants.USER_ID, ""))) {
                                    intent.putExtra("to_uid", roomsData.getFrom_uid());
                                } else {
                                    intent.putExtra("to_uid", roomsData.getTo_uid());
                                }
                            } else {
                                intent.putExtra("DISPLAY_NAME", contactsData.getDisplayName());
                                intent.putExtra("to_uid", contactsData.getId());
                            }
                            counter++;
                            startActivity(intent);
                        }
                    }
                });
            }
        });
        
        contactsRecycler.setAdapter(adapter);
        
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactsViewModel.deleteAll();
                requestContactPermission(true);
            }
        });
    }
    
    public void requestContactPermission(boolean forceRefresh) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Read contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("Please enable access to contacts.");
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {android.Manifest.permission.READ_CONTACTS}
                                    , PERMISSIONS_REQUEST_READ_CONTACTS);
                        }
                    });
                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_CONTACTS},
                            PERMISSIONS_REQUEST_READ_CONTACTS);
                }
            } else {
                contactsViewModel.getAllContacts(forceRefresh).observe(this, new Observer<List<ContactsData>>() {
                    @Override
                    public void onChanged(List<ContactsData> contactsData) {
                        adapter.setContacts(contactsData);
                    }
                });
            }
        } else {
            contactsViewModel.getAllContacts(forceRefresh).observe(this, new Observer<List<ContactsData>>() {
                @Override
                public void onChanged(List<ContactsData> contactsData) {
                    adapter.setContacts(contactsData);
                }
            });
        }
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    contactsViewModel.getAllContacts(false).observe(this, new Observer<List<ContactsData>>() {
                        @Override
                        public void onChanged(List<ContactsData> contactsData) {
                            adapter.setContacts(contactsData);
                        }
                    });
                } else {
                    Toast.makeText(this, "You have disabled the contacts permission", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }
}
