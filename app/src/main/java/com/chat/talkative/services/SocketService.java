package com.chat.talkative.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.telephony.PhoneNumberUtils;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleService;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.chat.talkative.activities.ChatActivity;
import com.chat.talkative.apiUtils.APIEndpoints;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.Talkative;
import com.chat.talkative.databases.entities.ChatsData;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.encryption.AES;
import com.chat.talkative.encryption.RSA;
import com.chat.talkative.interfaces.OnRoomCreatedListener;
import com.chat.talkative.repositories.ChatsRepo;
import com.chat.talkative.repositories.ContactsRepo;
import com.chat.talkative.repositories.RoomsRepo;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SocketService extends Service {
    Socket mSocket;
    Talkative db;
    
    private ChatsRepo chatsRepo;
    private RoomsRepo roomsRepo;
    private boolean isSocketConnected = false;
    
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Constants.IS_SOCKET_ACTIVE = true;
        /*if (mSocket == null) {
            init();
        }*/
        
        if (db == null) {
            initDB();
        }
        
        String action = intent.getAction();
        switch (action) {
            case "CONNECT":
                if (isConnected()) {
                    Log.e("SOCKET", "Connected to socket");
                } else {
                    init();
                    if (isConnected())
                        Log.e("SOCKET", "Connected to socket");
                    else
                        Log.e("SOCKET", "Failed to connect to socket");
                }
                break;
            
            case "JOIN":
                try {
                    join(intent.getStringExtra("room_id"));
                } catch (JSONException e) {
                    Log.e("SOCKET", "Something went wrong while joining. Check server logs...");
                }
                break;
            
            case "SEND":
                sendMessage(
                        intent.getStringExtra("id"),
                        intent.getStringExtra("message"),
                        intent.getStringExtra("to_uid"),
                        intent.getStringExtra("from_uid"),
                        intent.getStringExtra("created"),
                        intent.getStringExtra("key"),
                        intent.getByteArrayExtra("IV"),
                        intent.getStringExtra("room_id"),
                        intent.getBooleanExtra("isFirstMessage", false));
                break;
            
            case "UPDATE_PUB_KEY":
                updatePubKey(intent.getStringExtra("pub_key"));
                break;
            
            case "UPDATE_ROOM_STATUS":
                sendRoomStatus(intent.getStringExtra("room_id"), intent.getStringExtra("room_status"));
                break;
                
            case "DESTROY":
                sendUserStatus(intent.getStringExtra("id"));
                break;
        }
        
        return START_NOT_STICKY;
    }
    
    boolean isConnected() {
        return isSocketConnected;
    }
    
    void initDB() {
        db = Talkative.getInstance(this);
    }
    
    void init() {
        try {
            chatsRepo = new ChatsRepo(getApplication());
            roomsRepo = new RoomsRepo(getApplication());
            IO.Options options = IO.Options.builder()
                    .setQuery("id=" + getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""))
                    .build();
            mSocket = IO.socket(APIEndpoints.HOST_NAME, options);
            mSocket.connect();
            mSocket.on("connect", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    isSocketConnected = true;
                    initListeners();
                }
            });
        } catch (URISyntaxException e) {
            Log.e("ERROR", e.toString());
        }
    }
    
    void join(String roomId) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("from_uid", getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""));
        jsonObject.put("roomID", roomId);
        
        mSocket.emit("join", jsonObject);
    }
    
    void receiveMessages() {
        mSocket.on("messageReceived", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject body = (JSONObject) args[0];
                Log.e("BODY", body.toString());
                JSONObject obj = body.optJSONObject("msg");
                String msg = obj.optString("message");
                String aes_key = obj.optString("aes_key");
                String created = obj.optString("created");
                String status = obj.optString("status");
                String isDeleted = obj.optString("isDeleted");
                String id = obj.optString("id");
                String from_uid = obj.optString("from_uid");
                String to_uid = obj.optString("to_uid");
                String room_id = obj.optString("roomId");
                byte[] IV = Base64.decode(obj.optString("iv"), Base64.DEFAULT);
                
                try {
                    ChatsData data = new ChatsData();
                    data.setMessage(decrypt(aes_key, msg, IV));
                    data.setId(id);
                    data.setIsDeleted(isDeleted);
                    data.setStatus(status);
                    data.setCreated(formatTime(created));
                    data.setFrom_uid(from_uid);
                    data.setRoom_id(room_id);
                    data.setTo_uid(to_uid);
                    
                    chatsRepo.insertOne(data);
                    roomsRepo.updateLastMessage(room_id, decrypt(aes_key, msg, IV));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    private String roomId = "";
    private LiveData<ChatsData> chatData;
    
    private String last_message_id = "";
    
    private String ackKey, ackIV;
    
    void acknowledgeMessage() {
        mSocket.on("acknowledgeMessage", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject body = (JSONObject) args[0];
                JSONObject msg = body.optJSONObject("msg");
                String id = msg.optString("id");
                ackKey = msg.optString("aes_key");
                ackIV = msg.optString("iv");
                
                last_message_id = id;
                
                roomId = msg.optString("roomId");
                
                chatsRepo.updateRoomId(id, msg.optString("roomId"));
                chatsRepo.updateStatus(id, msg.optString("status"));
                new Handler(Looper.getMainLooper())
                        .post(new Runnable() {
                            @Override
                            public void run() {
                                chatsRepo.getOne(id).observeForever(chatsDataObserver3);
                            }
                        });
                
                Intent intent = new Intent("RoomCreated");
                intent.putExtra("roomId", roomId);
                
                LocalBroadcastManager.getInstance(SocketService.this)
                        .sendBroadcast(intent);
            }
        });
    }
    
    private final Observer<ChatsData> chatsDataObserver3 = new Observer<ChatsData>() {
        @Override
        public void onChanged(ChatsData chatsData) {
            roomsRepo.updateLastMessage(roomId, chatsData.getMessage());
        }
    };
    
    private LiveData<ContactsData> contactsDataLiveData;
    private String message = "";
    private String chatId = "";
    
    private Observer<ChatsData> chatsDataObserver2 = new Observer<ChatsData>() {
        @Override
        public void onChanged(ChatsData chatsData) {
            message = chatsData.getMessage();
            chatId = chatsData.getId();
            contactsDataLiveData = contactsRepo.getOneUsingId(newFromUid);
            contactsDataLiveData.observeForever(contactsObserver);
            chatData.removeObserver(this);
        }
    };
    
    private LiveData<RoomsData> roomsDataLiveData;
    
    private ChatsData newChatData;
    
    private Observer<ChatsData> chatsDataObserver = new Observer<ChatsData>() {
        @Override
        public void onChanged(ChatsData chatsData) {
            SocketService.this.newChatData = chatsData;
            roomsRepo.updateLastMessage(roomId, chatsData.getMessage());
            roomsDataLiveData = roomsRepo.getOneUsingId(roomId);
            roomsDataLiveData.observeForever(roomsDataObserver);
            chatData.removeObserver(this);
        }
    };
    
    private Observer<RoomsData> roomsDataObserver = new Observer<RoomsData>() {
        @Override
        public void onChanged(RoomsData roomsData) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("id", roomsData.getId());
                obj.put("to_uid", roomsData.getTo_uid());
                obj.put("from_uid", roomsData.getFrom_uid());
                obj.put("last_message_id", last_message_id);
                obj.put("phNum", Constants.SHARED_PREFERENCES.getString(Constants.USER_PH, ""));
                obj.put("created", roomsData.getCreated());
                
                JSONObject chatObj = new JSONObject();
                chatObj.put("id", newChatData.getId());
                chatObj.put("message", enc_message);
                chatObj.put("to_uid", newChatData.getTo_uid());
                chatObj.put("from_uid", newChatData.getFrom_uid());
                chatObj.put("created", newChatData.getCreated());
                chatObj.put("aes_key", ackKey);
                chatObj.put("status", "sent");
                chatObj.put("room_id", newChatData.getRoom_id());
                chatObj.put("isDeleted", newChatData.getIsDeleted());
                chatObj.put("iv", ackIV);
                
                obj.put("chatData", chatObj);
                
                mSocket.emit("roomCreated", obj);
                roomsDataLiveData.removeObserver(this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    
    private Observer<RoomsData> roomsDataObserver2 = new Observer<RoomsData>() {
        @Override
        public void onChanged(RoomsData roomsData) {
            if (roomsData == null) {
                SocketService.this.roomsData.setId(newRoomId);
                SocketService.this.roomsData.setTo_uid(newToUid);
                SocketService.this.roomsData.setFrom_uid(newFromUid);
                SocketService.this.roomsData.setCreated(newCreated);
                
                chatData = chatsRepo.getOne(last_message_id);
                chatData.observeForever(chatsDataObserver2);
                roomsDataLiveData.removeObserver(this);
            }
        }
    };
    
    RoomsData roomsData = new RoomsData();
    String newRoomId, newToUid, newFromUid, newCreated;
    String phNum = "";
    ContactsRepo contactsRepo = new ContactsRepo(getApplication());
    
    void handleNewRoomCreation() {
        mSocket.on("addNewRoom", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
                JSONObject msg = obj.optJSONObject("msg");
                JSONObject chatObj = msg.optJSONObject("chatData");
                
                String message = chatObj.optString("message");
                String aes_key = chatObj.optString("aes_key");
                String created = chatObj.optString("created");
                String status = chatObj.optString("status");
                String isDeleted = chatObj.optString("isDeleted");
                String id = chatObj.optString("id");
                String from_uid = chatObj.optString("from_uid");
                String to_uid = chatObj.optString("to_uid");
                chatObj.optString("room_id");
                String room_id = chatObj.optString("room_id").length() > 0 ? chatObj.optString("room_id") : msg.optString("id");
                byte[] IV = Base64.decode(chatObj.optString("iv"), Base64.DEFAULT);
                
                try {
                    ChatsData data = new ChatsData();
                    data.setMessage(decrypt(aes_key, message, IV));
                    data.setId(id);
                    data.setIsDeleted(isDeleted);
                    data.setStatus(status);
                    data.setCreated(formatTime(created));
                    data.setFrom_uid(from_uid);
                    data.setRoom_id(room_id);
                    data.setTo_uid(to_uid);
                    
                    chatsRepo.insertOne(data);
                    
                    newRoomId = msg.optString("id");
                    
                    chatsRepo.updateRoomId(chatId, newRoomId);
                    
                    newToUid = msg.optString("to_uid");
                    newFromUid = msg.optString("from_uid");
                    newCreated = msg.optString("created");
                    phNum = msg.optString("phNUm");
                    last_message_id = msg.optString("last_message_id");
                    
                    roomsDataLiveData = roomsRepo.getOneUsingId(msg.optString("id"));
                    
                    new Handler(Looper.getMainLooper())
                            .postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    roomsDataLiveData.observeForever(roomsDataObserver2);
                                    chatData = chatsRepo.getOne(last_message_id);
                                    chatData.observeForever(chatsDataObserver2);
                                }
                            }, 500);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    private Observer<ContactsData> contactsObserver = new Observer<ContactsData>() {
        @Override
        public void onChanged(ContactsData contactsData) {
            if (contactsData != null)
                roomsData.setTo_name(contactsData.getDisplayName());
            else
                roomsData.setTo_name(PhoneNumberUtils.formatNumber(phNum));
            
            roomsRepo.insertOne(roomsData);
            roomsRepo.joinRoom(roomsData);
            
            roomsRepo.updateLastMessage(newRoomId, message);
            contactsDataLiveData.removeObserver(this);
        }
    };
    
    void sendRoomStatus(String room_id, String room_status) {
        try {
            JSONObject body = new JSONObject();
            body.put("room_id", room_id);
            body.put("room_status", room_status);
            
            mSocket.emit("roomStatusSent", body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    void handleRoomStatus() {
        mSocket.on("receivedRoomStatus", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject body = (JSONObject) args[0];
                JSONObject msg = body.optJSONObject("msg");
                chatsRepo.updateRoomStatus(msg.optString("room_id"), msg.optString("room_status"));
                
                Intent intent = new Intent("RoomStatusUpdated");
                intent.putExtra("room_id", msg.optString("room_id"));
                intent.putExtra("room_status", msg.optString("room_status"));
                LocalBroadcastManager.getInstance(SocketService.this).sendBroadcast(intent);
            }
        });
    }
    
    void handleUserStatusUpdate() {
        mSocket.on("userStatusUpdated", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject body = (JSONObject) args[0];
                    JSONObject msg = body.optJSONObject("msg");
                    String status = msg.optString("status");
                    if(Character.isDigit(status.charAt(0))){
                        status = "last seen at " + formatTime(status);
                    }
                    contactsRepo.updateStatus(msg.optString("id"), status);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    void sendUserStatus(String id){
        try {
            Log.e("DISCONNECTED", "DISCONNECT");
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String currentTime = sdf.format(new Date());
        
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", id);
            jsonObject.put("status", currentTime);
        
            if (mSocket == null)
                init();
            mSocket.emit("disconnected", jsonObject);
        } catch (JSONException e) {
            Log.e("ERROR", e.toString());
        }
        //mSocket.close();
        //Constants.IS_SOCKET_ACTIVE = false;
    }
    
    void initListeners() {
        receiveMessages();
        receiveUpdatedPubKey();
        acknowledgeMessage();
        handleNewRoomCreation();
        handleRoomStatus();
        handleUserStatusUpdate();
    }
    
    String formatTime(String time) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = df.parse(time);
        df.setTimeZone(TimeZone.getDefault());
        return df.format(date);
    }
    
    void updatePubKey(String pub_key) {
        try {
            JSONObject data = new JSONObject();
            data.put("pub_key", pub_key);
            data.put("id", getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""));
            
            mSocket.emit("updatePubKey", data);
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }
        
    }
    
    void receiveUpdatedPubKey() {
        mSocket.on("pubKeyUpdated", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject) args[0];
                JSONObject msg = obj.optJSONObject("msg");
                
                db.contactsDAO().updatePubKey(msg.optString("id"), msg.optString("pub_key"));
                
                Intent intent = new Intent("UpdatedPubKey");
                intent.putExtra("pub_key", msg.optString("pub_key"));
                LocalBroadcastManager.getInstance(SocketService.this).sendBroadcast(intent);
            }
        });
    }
    
    private String enc_message = "";
    
    void sendMessage(String id, String message, String to_uid, String from_uid, String created, String key, byte[] IV, String room_id, boolean isFirstMessage) {
        enc_message = message;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", id + "");
            jsonObject.put("message", message);
            jsonObject.put("to_uid", to_uid);
            jsonObject.put("from_uid", from_uid);
            jsonObject.put("created", created);
            jsonObject.put("aes_key", key);
            jsonObject.put("room_id", room_id);
            jsonObject.put("iv", Base64.encodeToString(IV, Base64.DEFAULT));
            
            mSocket.emit("messageSent", jsonObject);
            
            roomId = room_id;
            last_message_id = id;
            ackKey = key;
            ackIV = Base64.encodeToString(IV, Base64.DEFAULT);
            if (isFirstMessage) {
                chatData = chatsRepo.getOne(id);
                new Handler(Looper.getMainLooper())
                        .post(new Runnable() {
                            @Override
                            public void run() {
                                chatData.observeForever(chatsDataObserver);
                            }
                        });
            }
        } catch (JSONException e) {
            Log.e("JSON", e.toString());
        }
    }
    
    String decrypt(String aesKey, String msg, byte[] IV) {
        try {
            byte[] decodeData = RSA.decryptByPrivateKey(Base64.decode(aesKey, Base64.DEFAULT), getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_PRV_KEY, ""));
            decodeData = new String(decodeData).replaceAll("\n", "").replaceAll(" ", "").getBytes();
            byte[] decodedKey = Base64.decode(decodeData, Base64.DEFAULT);
            SecretKey aes = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
            return AES.decrypt(Base64.decode(msg, Base64.DEFAULT), aes, IV);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String currentTime = sdf.format(new Date());
            
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""));
            jsonObject.put("time", currentTime);
            
            if (mSocket == null)
                init();
            mSocket.emit("disconnected", jsonObject);
        } catch (JSONException e) {
            Log.e("ERROR", e.toString());
        }
        mSocket.close();
        Constants.IS_SOCKET_ACTIVE = false;
        if (chatData != null) {
            chatData.removeObserver(chatsDataObserver);
            chatData.removeObserver(chatsDataObserver2);
        }
        if (roomsDataLiveData != null)
            roomsDataLiveData.removeObserver(roomsDataObserver);
        if (contactsDataLiveData != null)
            contactsDataLiveData.removeObserver(contactsObserver);
        
    }
    
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        try {
            Log.e("SERVICE", "DESTROYED");
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String currentTime = sdf.format(new Date());
            
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, MODE_PRIVATE).getString(Constants.USER_ID, ""));
            jsonObject.put("time", currentTime);
            
            if (mSocket == null)
                init();
            mSocket.emit("disconnected", jsonObject);
        } catch (JSONException e) {
            Log.e("ERROR", e.toString());
        }
        mSocket.close();
        Constants.IS_SOCKET_ACTIVE = false;
        if (chatData != null) {
            chatData.removeObserver(chatsDataObserver);
            chatData.removeObserver(chatsDataObserver2);
        }
        if (roomsDataLiveData != null)
            roomsDataLiveData.removeObserver(roomsDataObserver);
        if (contactsDataLiveData != null)
            contactsDataLiveData.removeObserver(contactsObserver);
        
    }
}
