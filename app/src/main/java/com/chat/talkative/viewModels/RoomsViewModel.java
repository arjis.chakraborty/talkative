package com.chat.talkative.viewModels;

import android.app.Application;
import android.content.Intent;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.chat.talkative.databases.Talkative;
import com.chat.talkative.databases.daos.RoomsDAO;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.repositories.RoomsRepo;
import com.chat.talkative.services.SocketService;

import java.util.List;

public class RoomsViewModel extends AndroidViewModel {
    private RoomsRepo roomsRepo;
    private LiveData<List<RoomsData>> roomsList;
    
    public RoomsViewModel(@NonNull Application application) {
        super(application);
        roomsRepo = new RoomsRepo(application);
        roomsList = roomsRepo.getAllRooms();
    }
    
    public void insertOne(RoomsData roomsData){
        roomsRepo.insertOne(roomsData);
    }
    
    public void updateLastMessage(String roomId, String lastMessage){
        roomsRepo.updateLastMessage(roomId, lastMessage);
    }
    
    public void deleteAll(){
        roomsRepo.deleteAll();
    }
    
    public LiveData<List<RoomsData>> getAllRooms(){
        return roomsList;
    }
    
    public void joinRoom(RoomsData roomsData){
        roomsRepo.joinRoom(roomsData);
    }
    
    public void getOne(String to_uid){
        roomsRepo.getOne(to_uid);
    }
    
    public void onDestroy(Intent intent){
        roomsRepo.onDestroy(intent);
    }
    
    @Override
    protected void onCleared() {
        super.onCleared();
        roomsRepo.removeObservers();
    }
}
