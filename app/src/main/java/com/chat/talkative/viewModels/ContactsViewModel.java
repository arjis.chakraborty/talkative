package com.chat.talkative.viewModels;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

import com.chat.talkative.databases.Talkative;
import com.chat.talkative.databases.daos.ContactsDAO;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.repositories.ContactsRepo;

import java.util.List;

public class ContactsViewModel extends AndroidViewModel {
    private ContactsRepo contactsRepo;
    private LiveData<List<ContactsData>> contactsList;
    
    public ContactsViewModel(@NonNull Application application) {
        super(application);
        contactsRepo = new ContactsRepo(application);
    }
    
    public void insertOne(ContactsData contactsData) {
        contactsRepo.insertOne(contactsData);
    }
    
    public LiveData<ContactsData> getOne(String phNum) {
        return contactsRepo.getOne(phNum);
    }
    
    public void updateDisplayName(String phNum, String displayName) {
        contactsRepo.updateDisplayName(phNum, displayName);
    }
    
    public void updatePicURL(String phNum, String picURL) {
        contactsRepo.updatePicURL(phNum, picURL);
    }
    
    public void deleteAll() {
        contactsRepo.deleteAll();
    }
    
    public LiveData<List<ContactsData>> getAllContacts(boolean forceRefresh) {
        return contactsRepo.getAllContacts(forceRefresh);
    }
    
    public LiveData<RoomsData> getRoom(String to_uid){
        return contactsRepo.getRoomDetails(to_uid);
    }
    
    /*public void createOrJoinRoom(String to_uid){
        contactsRepo.createOrJoinRoom(to_uid);
    }*/
    
    @Override
    protected void onCleared() {
        super.onCleared();
        contactsRepo.removeObservers();
    }
}

