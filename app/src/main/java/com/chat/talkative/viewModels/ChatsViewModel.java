package com.chat.talkative.viewModels;

import android.app.Application;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.PagedList;

import com.chat.talkative.databases.entities.ChatsData;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.repositories.ChatsRepo;

import java.util.List;

public class ChatsViewModel extends AndroidViewModel {
    
    private ChatsRepo chatsRepo;
    
    public ChatsViewModel(@NonNull Application application) {
        super(application);
        chatsRepo = new ChatsRepo(application);
    }
    
    public long insertOne(ChatsData chatsData){
        return chatsRepo.insertOne(chatsData);
    }
    
    public void updateStatus(String id, String status){
       chatsRepo.updateStatus(id, status);
    }
    
    public void deleteAll(){
        chatsRepo.deleteAll();
    }
    
    public LiveData<PagedList<ChatsData>> getAllChats(String roomId){
        return chatsRepo.getAllChats(roomId);
    }
    
    public LiveData<ContactsData> getContactsDetails(String id){
        return chatsRepo.getContactDetails(id);
    }
    
    public void createRoomAndSendMessage(String phNum, Intent intent){
        chatsRepo.createRoomAndSendMessage(phNum, intent);
    }
    
    public void joinRoom(String roomId){
        chatsRepo.joinRoom(roomId);
    }
    
    public void sendMessage(Intent intent){
        chatsRepo.sendMessage(intent);
    }
    
    public void sendRoomStatus(String room_id, String room_status){
        chatsRepo.sendRoomStatus(room_id, room_status);
    }
    
    @Override
    protected void onCleared() {
        super.onCleared();
        chatsRepo.removeObservers();
    }
}
