package com.chat.talkative.encryption;

import android.util.Base64;

import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

/*public class RSA {
    
    public static BigInteger publicModulus;
    public static BigInteger publicExponent;
    
    public static BigInteger privateModulus;
    public static BigInteger privateExponent;
    
    
     *//*Generationg Rsa key public and private modulus and exponetial and saving them
     * i am saving them here as static variable
     * for future refrence you can also save them anywhere else you like*//*
    public static void generateAndSaveRsaKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024); // you can decrease and increase this as per requirement
        KeyPair kp = kpg.genKeyPair();
        Key publicKey = kp.getPublic();
        Key privateKey = kp.getPrivate();
        
        KeyFactory fact = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec pub = fact.getKeySpec(publicKey,
                RSAPublicKeySpec.class);
        RSAPrivateKeySpec priv = fact.getKeySpec(privateKey,
                RSAPrivateKeySpec.class);
        
        publicModulus = pub.getModulus();
        publicExponent = pub.getPublicExponent();
        privateModulus = priv.getModulus();
        privateExponent = priv.getPrivateExponent();
        
    }
    
    public static PublicKey readPublicKey() throws IOException {
        
        try {
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(publicModulus, publicExponent);
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey pubKey = fact.generatePublic(keySpec);
            return pubKey;
        } catch (Exception e) {
            throw new RuntimeException("Spurious serialisation error", e);
        }
    }
    
    public static PrivateKey readPrivateKey() throws IOException {
        
        try {
            RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(privateModulus, privateExponent);
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PrivateKey priKey = fact.generatePrivate(keySpec);
            return priKey;
        } catch (Exception e) {
            throw new RuntimeException("Spurious serialisation error", e);
        }
    }
    
    public static String rsaEncrypt(String data) throws NoSuchPaddingException, NoSuchAlgorithmException, IOException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        PublicKey pubKey = readPublicKey();
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] cipherData = cipher.doFinal(data.getBytes());
        return Base64.encodeToString(cipherData, 0);
    }
    
    public static String rsaDecrypt(String data) throws NoSuchPaddingException, NoSuchAlgorithmException, IOException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        PrivateKey pubKey = readPrivateKey();
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, pubKey);
        byte[] cipherData = cipher.doFinal(Base64.decode(data, 0));
        return new String(cipherData);
    }
} */

public class RSA {
    public static final String KEY_ALGORITHM = "RSA";
    public static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
    
    private static final String PUBLIC_KEY = "RSAPublicKey";
    private static final String PRIVATE_KEY = "RSAPrivateKey";
    
    public static byte[] decryptBASE64(String key) throws Exception {
        return Base64.decode(key, Base64.DEFAULT);
    }
    
    public static String encryptBASE64(byte[] key) throws Exception {
        return Base64.encodeToString(key, Base64.DEFAULT);
    }
    
    public static String sign(byte[] data, String privateKey) throws Exception {
        
        byte[] keyBytes = decryptBASE64(privateKey);
        
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        
        PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);
        
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(priKey);
        signature.update(data);
        
        return encryptBASE64(signature.sign());
    }
    
    public static boolean verify(byte[] data, String publicKey, String sign)
            throws Exception {
        
        byte[] keyBytes = decryptBASE64(publicKey);
        
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        
        PublicKey pubKey = keyFactory.generatePublic(keySpec);
        
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initVerify(pubKey);
        signature.update(data);
        
        return signature.verify(decryptBASE64(sign));
    }
    
    public static byte[] decryptByPrivateKey(byte[] data, String key)
            throws Exception {
        
        byte[] keyBytes = decryptBASE64(key);
        
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        
        Cipher cipher = Cipher.getInstance(/*keyFactory.getAlgorithm()*/"RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        
        return cipher.doFinal(data);
    }
    
    public static byte[] decryptByPublicKey(byte[] data, String key)
            throws Exception {
        
        byte[] keyBytes = decryptBASE64(key);
        
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key publicKey = keyFactory.generatePublic(x509KeySpec);
        
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        
        return cipher.doFinal(data);
    }
    
    public static byte[] encryptByPublicKey(byte[] data, String key)
            throws Exception {
        byte[] keyBytes = decryptBASE64(key);
        
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key publicKey = keyFactory.generatePublic(x509KeySpec);
        
        Cipher cipher = Cipher.getInstance(/*keyFactory.getAlgorithm()*/"RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        
        return cipher.doFinal(data);
    }
    
    public static byte[] encryptByPrivateKey(byte[] data, String key)
            throws Exception {
        
        byte[] keyBytes = decryptBASE64(key);
        
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        
        return cipher.doFinal(data);
    }
    
    public static String getPrivateKey(Map<String, Object> keyMap)
            throws Exception {
        Key key = (Key) keyMap.get(PRIVATE_KEY);
        
        return encryptBASE64(key.getEncoded());
    }
    
    public static String getPublicKey(Map<String, Object> keyMap)
            throws Exception {
        Key key = (Key) keyMap.get(PUBLIC_KEY);
        
        return encryptBASE64(key.getEncoded());
    }
    
    public static Map<String, Object> initKey() throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator
                .getInstance(KEY_ALGORITHM);
        keyPairGen.initialize(2048);
        
        KeyPair keyPair = keyPairGen.generateKeyPair();
        
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        
        Map<String, Object> keyMap = new HashMap<String, Object>(2);
        
        keyMap.put(PUBLIC_KEY, publicKey);
        keyMap.put(PRIVATE_KEY, privateKey);
        return keyMap;
    }
    
    public static PublicKey getPublicKey(String MODULUS, String EXPONENT) throws Exception {
        byte[] modulusBytes = Base64.decode(MODULUS, 0);
        byte[] exponentBytes = Base64.decode(EXPONENT, 0);
        
        BigInteger modulus = new BigInteger(1, (modulusBytes));
        BigInteger exponent = new BigInteger(1, (exponentBytes));
        
        RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
        KeyFactory kf = KeyFactory.getInstance(RSA.KEY_ALGORITHM);
        return kf.generatePublic(spec);
    }
    
    public static byte[] encrypt(Key publicKey, String s) throws Exception {
        byte[] byteData = s.getBytes();
        Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedData = cipher.doFinal(byteData);
        
        
        return encryptedData;
    }
    
}
