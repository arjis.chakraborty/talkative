package com.chat.talkative.encryption;

import android.util.Base64;
import android.util.Log;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
    public static KeyGenerator keyGenerator;
    public static SecretKey secretKey;
    public static byte[] IV = new byte[16];
    public static SecureRandom random;
    public static IvParameterSpec ivSpec;
    
    public static void init() {
        try {
            keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(128);
            secretKey = keyGenerator.generateKey();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void initPadding() {
        random = new SecureRandom();
        random.nextBytes(IV);
        //ivSpec = new IvParameterSpec(IV);
    }
    
    public static byte[] encrypt(byte[] plaintext, SecretKey key, byte[] IV) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(IV);
        cipher.init(Cipher.ENCRYPT_MODE, /*keySpec*/key, ivSpec);
        byte[] cipherText = cipher.doFinal(plaintext);
        return cipherText;
    }
    
    public static String decrypt(byte[] cipherText, SecretKey key, byte[] IV) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(IV);
            cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
            Log.e("BLOCK", new String(cipherText) + ", " + cipherText.length);
            byte[] decryptedText = cipher.doFinal(cipherText);
            return new String(decryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static String getStringKey(SecretKey key) {
        SecretKey secretKey;
        String stringKey;
    
        secretKey = key;
    
        if (secretKey != null) {
            stringKey = Base64.encodeToString(secretKey.getEncoded(), Base64.DEFAULT);
            return stringKey;
        }
        else
            return null;
    }
    
    /*public static String encrypt(String message, SecretKey secret)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
        return Base64.encodeToString(cipherText, Base64.NO_WRAP);
    }
    
    public static String decrypt(String cipherText, SecretKey secret)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret);
        byte[] decode = Base64.decode(cipherText, Base64.NO_WRAP);
        Log.e("BLOCK", new String(decode) + ", " + decode.length);
        String decryptString = new String(cipher.doFinal(decode), "UTF-8");
        return decryptString;
    }*/
}


/*public class AES {
    public static String AES_ALGORITHM = "AES";
    public static String AES_TRANSFORMATION = "AES/ECB/PKCS5Padding";
    
    
    private static SecretKeySpec secretKey;
    private static byte[] key;
    
    public static void setKey(String myKey)
    {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, AES_ALGORITHM);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    
    public static String encrypt(String strToEncrypt, String secret)
    {
        try
        {
            setKey(secret);
            Cipher cipher = Cipher.getInstance(AES_TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] inputBytes = cipher.doFinal(strToEncrypt.getBytes());
            return Base64Utils.encodeToString(inputBytes);
        }
        catch (Exception e)
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
    
    public static String decrypt(String strToDecrypt*//*, String secret*//*)
    {
        try
        {
            
            // setKey(secret); // this used to get encrypted key if doing data decryption without encrypted key knowledg
            // or if don't know encryption key used to encrypted data previously
            
            Cipher cipher = Cipher.getInstance(AES_TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            
            byte[] inputBytes = cipher.doFinal(Base64Utils.decodeFromString(strToDecrypt));
            return new String(inputBytes);
            
        }
        catch (Exception e)
        {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }
}*/