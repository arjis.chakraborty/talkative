package com.chat.talkative.apiUtils;

public class APIEndpoints {
    
    public static final String HOST_NAME = "http://192.168.0.3:3000/";
    //public static final String HOST_NAME = "http://b32ba9fe7d04.ngrok.io/";
    
    private static final String MAIN_ROUTE = "api/";
    public static String SEND_OTP = HOST_NAME + MAIN_ROUTE + "sendOTP/";
    public static String VERIFY_OTP = HOST_NAME + MAIN_ROUTE + "verifyOTP/";
    
    public static final String ADD_USER = HOST_NAME + MAIN_ROUTE + "addUser/";
    public static final String GET_USER = HOST_NAME + MAIN_ROUTE + "getUser/";
    public static final String GET_ALL_USERS = HOST_NAME + MAIN_ROUTE + "getAllUsers/";
    public static final String GET_ALL_CONTACTS = HOST_NAME + MAIN_ROUTE + "getAllContacts/";
    public static final String CHECK_USER = HOST_NAME + MAIN_ROUTE + "checkUser/";
    public static final String UPDATE_USER = HOST_NAME + MAIN_ROUTE + "updateUser/";
    
    public static final String GET_ROOM = HOST_NAME + MAIN_ROUTE + "getRoom/";
    public static final String GET_ALL_ROOMS = HOST_NAME + MAIN_ROUTE + "getAllRooms/";
    public static final String CREATE_ROOM = HOST_NAME + MAIN_ROUTE + "createRoom/";
    
    public static final String GET_ALL_NEW_MESSAGES = HOST_NAME + MAIN_ROUTE + "getAllNewMessages/";
}
