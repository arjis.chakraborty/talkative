package com.chat.talkative.apiUtils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chat.talkative.interfaces.OnResponse;


import org.json.JSONObject;

public class RequestAPI {
    String endpoint;
    Context context;
    
    public RequestAPI(String endpoint, Context context) {
        this.endpoint = endpoint;
        this.context = context;
    }
    
    public void post(JSONObject jsonObject, OnResponse listener) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String URL = endpoint;
        
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponseReceived(response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onErrorReceived(error.toString());
            }
        });
        requestQueue.add(request);
    }
}
