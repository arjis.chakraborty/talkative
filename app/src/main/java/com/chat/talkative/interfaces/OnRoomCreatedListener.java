package com.chat.talkative.interfaces;


public interface OnRoomCreatedListener {
    void onRoomCreated(String roomId);
}
