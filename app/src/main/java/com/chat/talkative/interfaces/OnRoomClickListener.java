package com.chat.talkative.interfaces;

import android.view.View;

import com.chat.talkative.databases.entities.RoomsData;

public interface OnRoomClickListener {
    void onClick(View view, /*int position*/RoomsData roomsData);
}
