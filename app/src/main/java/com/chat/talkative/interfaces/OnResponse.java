package com.chat.talkative.interfaces;

public interface OnResponse {
    void onResponseReceived(String response);
    void onErrorReceived(String error);
}
