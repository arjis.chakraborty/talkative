package com.chat.talkative.interfaces;

import com.chat.talkative.databases.entities.ContactsData;

public interface OnContactClickListener {
    void onClick(ContactsData contactsData);
}
