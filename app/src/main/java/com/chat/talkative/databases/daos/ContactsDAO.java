package com.chat.talkative.databases.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.chat.talkative.databases.entities.ContactsData;

import java.util.List;

@Dao
public interface ContactsDAO {
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOne(ContactsData contactsData);
    
    @Query("SELECT * FROM contacts ORDER BY displayName ASC")
    LiveData<List<ContactsData>> getAllContacts();
    
    @Query("SELECT * FROM contacts WHERE phNum LIKE :phNum LIMIT 1")
    LiveData<ContactsData> getOne(String phNum);
    
    @Query("DELETE FROM contacts")
    void deleteAll();
    
    @Query("UPDATE contacts SET displayName = :displayName WHERE phNum = :phNum")
    void updateDisplayName(String phNum, String displayName);
    
    @Query("UPDATE contacts SET picURL =:picURL WHERE phNUm = :phNum")
    void updatePicUrl(String phNum, String picURL);
    
    @Query("UPDATE contacts SET pub_key =:pub_key WHERE id = :id")
    void updatePubKey(String id, String pub_key);
    
    @Query("UPDATE contacts SET status = :status WHERE id = :id")
    void updateStatus(String id, String status);
    
    @Query("SELECT * FROM contacts WHERE id = :id LIMIT 1")
    LiveData<ContactsData> getOneUsingId(String id);
}
