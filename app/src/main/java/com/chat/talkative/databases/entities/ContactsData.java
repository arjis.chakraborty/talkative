package com.chat.talkative.databases.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "contacts")
public class ContactsData {
    
    @PrimaryKey(autoGenerate = false)
    @NonNull
    private String id;
    
    private String name;
    
    private String phNum;
    
    private String picURL;
    
    private String displayName;
    
    private String pub_key;
    
    private String logged_in;
    
    private String status;
    
    private String created;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getPhNum() {
        return phNum;
    }
    
    public void setPhNum(String phNum) {
        this.phNum = phNum;
    }
    
    public String getPicURL() {
        return picURL;
    }
    
    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }
    
    public String getDisplayName() {
        return displayName;
    }
    
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    public String getPub_key() {
        return pub_key;
    }
    
    public void setPub_key(String pub_key) {
        this.pub_key = pub_key;
    }
    
    public String getLogged_in() {
        return logged_in;
    }
    
    public void setLogged_in(String logged_in) {
        this.logged_in = logged_in;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getCreated() {
        return created;
    }
    
    public void setCreated(String created) {
        this.created = created;
    }
}
