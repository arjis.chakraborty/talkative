package com.chat.talkative.databases;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.daos.ChatsDAO;
import com.chat.talkative.databases.daos.ContactsDAO;
import com.chat.talkative.databases.daos.RoomsDAO;
import com.chat.talkative.databases.entities.ChatsData;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;

@Database(entities = {ContactsData.class, RoomsData.class, ChatsData.class}, exportSchema = false, version = 14)
public abstract class Talkative extends RoomDatabase {
    private static Talkative roomDB;
    private static final String DATABASE_NAME = Constants.DB_NAME;
    
    public synchronized static Talkative getInstance(Context context) {
        if (roomDB == null) {
            roomDB = Room.databaseBuilder(context,
                    Talkative.class,
                    DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return roomDB;
    }
    
    public abstract ContactsDAO contactsDAO();
    public abstract RoomsDAO roomsDAO();
    public abstract ChatsDAO chatsDAO();
}