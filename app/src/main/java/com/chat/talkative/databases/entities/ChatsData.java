package com.chat.talkative.databases.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "messages")
public class ChatsData {
    
    @PrimaryKey
    @NonNull
    private String id;
    
    private String message;
    
    //private String aes_key;
    
    //private String iv;
    
    private String status;
    
    private String from_uid;
    
    private String to_uid;
    
    private String isDeleted;
    
    private String room_id;
    
    private String created;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    /*public String getAes_key() {
        return aes_key;
    }
    
    public void setAes_key(String aes_key) {
        this.aes_key = aes_key;
    }
    
    public String getIv() {
        return iv;
    }
    
    public void setIv(String iv) {
        this.iv = iv;
    }*/
    
    public String getIsDeleted() {
        return isDeleted;
    }
    
    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    public String getFrom_uid() {
        return from_uid;
    }
    
    public void setFrom_uid(String from_uid) {
        this.from_uid = from_uid;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getTo_uid() {
        return to_uid;
    }
    
    public void setTo_uid(String to_uid) {
        this.to_uid = to_uid;
    }
    
    public String getRoom_id() {
        return room_id;
    }
    
    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }
    
    public String getCreated() {
        return created;
    }
    
    public void setCreated(String created) {
        this.created = created;
    }
}
