package com.chat.talkative.databases.daos;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.paging.PagedList;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.chat.talkative.databases.entities.ChatsData;

import java.util.List;

@Dao
public interface ChatsDAO {
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOne(ChatsData chatsData);
    
    @Query("UPDATE messages SET status = :status WHERE messages.id = :id")
    void updateStatus(String id, String status);
    
    @Query("UPDATE messages SET room_id = :roomId WHERE messages.id = :id")
    void updateRoomId(String id, String roomId);
    
    @Query("DELETE FROM messages")
    void deleteAll();
    
    @Query("SELECT * FROM messages WHERE room_id = :roomId ORDER BY id DESC")
    DataSource.Factory<Integer, ChatsData> getAllChats(String roomId);
    
    @Query("SELECT * FROM messages WHERE id = :id LIMIT 1")
    LiveData<ChatsData> getOne(String id);
}
