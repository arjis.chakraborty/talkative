package com.chat.talkative.databases.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "rooms")
public class RoomsData {
    
    @PrimaryKey(autoGenerate = false)
    @NonNull
    private String id;
    
    private String to_uid;
    
    private String from_uid;
    
    private String to_name;
    
    private String last_message;
    
    @ColumnInfo(name = "room_status", defaultValue = "")
    private String room_Status;
    
    private String created;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getTo_name() {
        return to_name;
    }
    
    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }
    
    public String getTo_uid() {
        return to_uid;
    }
    
    public void setTo_uid(String to_uid) {
        this.to_uid = to_uid;
    }
    
    public String getRoom_Status() {
        return room_Status;
    }
    
    public void setRoom_Status(String room_Status) {
        this.room_Status = room_Status;
    }
    
    public String getFrom_uid() {
        return from_uid;
    }
    
    public void setFrom_uid(String from_uid) {
        this.from_uid = from_uid;
    }
    
    public String getLast_message() {
        return last_message;
    }
    
    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }
    
    public String getCreated() {
        return created;
    }
    
    public void setCreated(String created) {
        this.created = created;
    }
}
