package com.chat.talkative.databases.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.databases.entities.RoomsData;

import java.util.List;

@Dao
public interface RoomsDAO {
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOne(RoomsData roomsData);
    
    @Query("UPDATE rooms SET last_message = :lastMessage WHERE rooms.id = :roomId")
    void updateLastMessage(String roomId, String lastMessage);
    
    @Query("DELETE FROM rooms")
    void deleteAll();
    
    @Query("SELECT * FROM rooms ORDER BY created DESC")
    LiveData<List<RoomsData>> getAllRooms();
    
    @Query("SELECT * FROM contacts WHERE id = :to_uid")
    ContactsData getOneContact(String to_uid);
    
    @Query("SELECT * FROM rooms WHERE to_uid = :to_uid OR from_uid = :to_uid")
    LiveData<RoomsData> getOneRoom(String to_uid);
    
    @Query("SELECT * FROM rooms WHERE id = :roomId")
    LiveData<RoomsData> getOneRoomUsingId(String roomId);
    
    @Query("UPDATE rooms SET room_status = :room_status WHERE id = :room_id")
    void updateRoomStatus(String room_id, String room_status);
}
