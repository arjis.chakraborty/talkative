package com.chat.talkative.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.chat.talkative.R;
import com.chat.talkative.constants.Constants;
import com.chat.talkative.databases.entities.ChatsData;

import java.util.ArrayList;
import java.util.List;

public class ChatsRecyclerAdapter extends PagedListAdapter<ChatsData, RecyclerView.ViewHolder> {
    
    List<ChatsData> messages;
    Context context;
    
    public ChatsRecyclerAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }
    
    private static final DiffUtil.ItemCallback<ChatsData> DIFF_CALLBACK = new DiffUtil.ItemCallback<ChatsData>() {
        @Override
        public boolean areItemsTheSame(@NonNull ChatsData oldItem, @NonNull ChatsData newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
        
        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull ChatsData oldItem, @NonNull ChatsData newItem) {
            return oldItem.getFrom_uid().equals(newItem.getFrom_uid()) &&
                    oldItem.getTo_uid().equals(newItem.getTo_uid()) &&
                    oldItem.getId().equals(newItem.getId()) &&
                    oldItem.getMessage().equals(newItem.getMessage()) &&
                    oldItem.getCreated().equals(newItem.getCreated()) &&
                    oldItem.getIsDeleted().equals(newItem.getIsDeleted()) &&
                    oldItem.getStatus().equals(newItem.getStatus()) &&
                    oldItem.getRoom_id().equals(newItem.getRoom_id());
        }
    };
    
    public void setChats(List<ChatsData> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }
    
    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null)
            if (getItem(position).getFrom_uid().equals(context.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(Constants.USER_ID, "")))
                return 1;
            else
                return 0;
            return -1;
    }
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new ReceivedViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_received_message, parent, false));
            
            case 1:
                return new SentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sent_message, parent, false));
                
            case -1:
                return new ErrorViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_not_retrieved, parent, false));
        }
        return null;
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ReceivedViewHolder receivedViewHolder = (ReceivedViewHolder) holder;
                receivedViewHolder.message.setText(getItem(position).getMessage());
                receivedViewHolder.time.setText(getItem(position).getCreated());
                break;
            
            case 1:
                SentViewHolder sentViewHolder = (SentViewHolder) holder;
                if (position != 0)
                    if (getItem(position).getFrom_uid().equals(context.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE).getString(Constants.USER_ID, "")))
                        if (getItem(position).getStatus().equalsIgnoreCase("pending"))
                            sentViewHolder.mainLayout.setBackgroundResource(R.drawable.chat_sent_message_pending);
                        else
                            sentViewHolder.mainLayout.setBackgroundResource(R.drawable.chat_sent_message);
                sentViewHolder.message.setText(getItem(position).getMessage());
                sentViewHolder.time.setText(getItem(position).getCreated());
                break;
                
            case -1:
                ErrorViewHolder errorViewHolder = (ErrorViewHolder) holder;
                break;
        }
    }
    
    public class ReceivedViewHolder extends RecyclerView.ViewHolder {
        
        TextView message, time;
        
        public ReceivedViewHolder(@NonNull View itemView) {
            super(itemView);
            
            message = itemView.findViewById(R.id.message);
            time = itemView.findViewById(R.id.time);
        }
    }
    
    public class SentViewHolder extends RecyclerView.ViewHolder {
        TextView message, time;
        ConstraintLayout mainLayout;
        
        public SentViewHolder(@NonNull View itemView) {
            super(itemView);
            
            mainLayout = itemView.findViewById(R.id.mainLayout);
            message = itemView.findViewById(R.id.message);
            time = itemView.findViewById(R.id.time);
        }
    }
    
    public class ErrorViewHolder extends RecyclerView.ViewHolder{
    
        public ErrorViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
