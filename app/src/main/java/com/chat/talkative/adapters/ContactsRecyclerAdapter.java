package com.chat.talkative.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chat.talkative.R;
import com.chat.talkative.databases.entities.ContactsData;
import com.chat.talkative.interfaces.OnContactClickListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsRecyclerAdapter extends RecyclerView.Adapter<ContactsRecyclerAdapter.ViewHolder> {
    List<ContactsData> dataList;
    Context context;
    
    OnContactClickListener listener;
    
    public ContactsRecyclerAdapter(Context context, OnContactClickListener listener) {
        this.listener = listener;
        this.context = context;
    }
    
    public void setContacts(List<ContactsData> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacts, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ContactsData data = dataList.get(position);
        
        holder.name.setText(data.getDisplayName());
        holder.messagePreview.setText("You will view my status here...");
    }
    
    public class ViewHolder extends RecyclerView.ViewHolder {
        
        TextView name, messagePreview;
        CircleImageView pic;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            
            name = itemView.findViewById(R.id.userName);
            messagePreview = itemView.findViewById(R.id.messagePreview);
            pic = itemView.findViewById(R.id.profile_image);
            
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(dataList.get(getAdapterPosition()));
                }
            });
        }
    }
}
