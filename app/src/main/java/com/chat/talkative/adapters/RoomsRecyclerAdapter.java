package com.chat.talkative.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chat.talkative.R;
import com.chat.talkative.databases.Talkative;
import com.chat.talkative.databases.entities.RoomsData;
import com.chat.talkative.interfaces.OnRoomClickListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RoomsRecyclerAdapter extends RecyclerView.Adapter<RoomsRecyclerAdapter.ViewHolder> {
    
    List<RoomsData> dataList;
    Context context;
    
    OnRoomClickListener listener;
    
    Talkative db;
    
    public RoomsRecyclerAdapter(Context context, OnRoomClickListener listener) {
        this.listener = listener;
        this.context = context;
        db = Talkative.getInstance(context);
    }
    
    public void setRooms(List<RoomsData> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rooms, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RoomsData data = dataList.get(position);
        
        holder.name.setText(data.getTo_name());
        holder.messagePreview.setText(data.getLast_message());
    
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(view, dataList != null && dataList.size() > 0 ? dataList.get(position) : null);
            }
        });
        
        if(data.getRoom_Status() != null && data.getRoom_Status().length() > 0){
            holder.currentRoomStatus.setVisibility(View.VISIBLE);
            holder.currentRoomStatus.setText(data.getRoom_Status());
            holder.messagePreview.setVisibility(View.GONE);
        }
        else{
            holder.currentRoomStatus.setVisibility(View.GONE);
            holder.messagePreview.setVisibility(View.VISIBLE);
        }
    }
    
    public class ViewHolder extends RecyclerView.ViewHolder {
        
        TextView name, messagePreview, currentRoomStatus;
        CircleImageView pic, blob;
        RelativeLayout mainLayout;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            
            mainLayout = itemView.findViewById(R.id.mainLayout);
            name = itemView.findViewById(R.id.userName);
            messagePreview = itemView.findViewById(R.id.messagePreview);
            pic = itemView.findViewById(R.id.profile_image);
            blob = itemView.findViewById(R.id.blob);
            currentRoomStatus = itemView.findViewById(R.id.currentRoomStatus);
        }
    }
}
